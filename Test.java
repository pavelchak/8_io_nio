package com.pavelchak.Model.Droids;

import com.pavelchak.Model.DTO.SampleBattle;
import com.pavelchak.Model.DroidBattle;
import com.pavelchak.Model.Droids.Weapons.WeaponInterface;

import java.io.Serializable;
import java.util.*;

public class BaseDroid implements Serializable {
    Random random;

    private String nameDroid;

    //characteristics
    private int maxHealth;      //point, health<=maxHealth
    private int health;         //point, if health=0, then it is dead
    private int strength;       //point, strength 1..20
    private int attack;         //20-100%, probability of strike (not used)
    private int armor;          //point, armor=1..10
    private int defence;        //20-100%, probability of escaping strike (not used)
    private int luck;           //20-100%, probability of a critical strike
    // example: shot = -20 -> strike
    // example: shot = +20 -> add health
    // updateShot = shot + shot * 0.2 * attackingStrength
    //
    // if(random.nextInt(100) + attackingDroid.getLuck() - 100 > 50)
    //    then damageForAttacked = updateShot
    // else damageForAttacked = updateShot + attackedArmor
    //      if(damageForAttacked>0) then damageForAttacked=0
    //
    // health = health + damageForAttacked
    //

    //weapon
    transient private WeaponInterface weapon1;
    transient private WeaponInterface weapon2;

    transient private List<BaseDroid> myGroup;
    transient private List<BaseDroid> enemyGroup;
    transient private DroidBattle.CurrentBattle currentBattle;

    BaseDroid(String name, List<BaseDroid> myGroup, List<BaseDroid> enemyGroup) {
        this.nameDroid = name;
        this.myGroup = myGroup;
        this.enemyGroup = enemyGroup;
    }

    public BaseDroid addCurrentBattle(DroidBattle.CurrentBattle currentBattle) {
        this.currentBattle = currentBattle;
        return this;
    }

    private BaseDroid kickWithWeapon(WeaponInterface weapon) {
        BaseDroid droid2;
        int i;
        if (weapon.kickSelfGroup()) {
            if (myGroup.size() == 1) {
                i = 0;
            } else {
                i = random.nextInt(myGroup.size() - 1);
            }
            droid2 = myGroup.get(i);
        } else {

            if (enemyGroup.size() == 1) {
                i = 0;
            } else {
                i = random.nextInt(enemyGroup.size() - 1);
            }
            droid2 = enemyGroup.get(i);
        }
        int shot = weapon.kickDroid(this, droid2, random);
        SampleBattle sampleBattle = new SampleBattle(currentBattle.getRound(), this, droid2, shot);
        currentBattle.getHistoryRound().add(sampleBattle);
        return droid2;
    }

    public BaseDroid kick(Random random) {
        this.random = random;
        if (random.nextBoolean()) {
            return kickWithWeapon(weapon1);
        } else {
            return kickWithWeapon(weapon2);
        }
    }

    @Override
    public String toString() {
        return nameDroid + "(H:" + health + " A:" + armor + ")";
    }

    //region geter-seter
    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getDefence() {
        return defence;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public int getLuck() {
        return luck;
    }

    public void setLuck(int luck) {
        this.luck = luck;
    }

    public WeaponInterface getWeapon1() {
        return weapon1;
    }

    public void setWeapon1(WeaponInterface weapon1) {
        this.weapon1 = weapon1;
    }

    public WeaponInterface getWeapon2() {
        return weapon2;
    }

    public void setWeapon2(WeaponInterface weapon2) {
        this.weapon2 = weapon2;
    }

    public String getNameDroid() {
        return nameDroid;
    }

    public void setNameDroid(String nameDroid) {
        this.nameDroid = nameDroid;
    }

    //endregion
}
