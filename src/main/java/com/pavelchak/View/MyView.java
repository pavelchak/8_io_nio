package com.pavelchak.View;

import java.io.*;
import java.util.*;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        menu.put("1", "  1 - Test usual and buffered reader");
        menu.put("2", "  2 - Read a Java source-code file");
        menu.put("3", "  3 - Show contents of a specific directory");

        menu.put("Q", "  Q - exit");

        methodsMenu.put("1", this::testReader);
        methodsMenu.put("2", this::testJavaFileOnCommentary);
        methodsMenu.put("3", this::showContentsDirectory);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values())
            System.out.println(str);
    }

    private void testReader() throws IOException {
        int count = 0;
        System.out.println("Test usual reader");
        InputStream inputstream =
                new FileInputStream("1.pdf");
        int data = inputstream.read();
        while (data != -1) {
            data = inputstream.read();
            count++;
        }
        inputstream.close();
        System.out.println("count=" + count);
        System.out.println("End test usual reader");

        System.out.println("Test buffered reader");
        DataInputStream in = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("1.pdf")));
        count = 0;
        try {
            while (true) {
                byte b = in.readByte();
                count++;
            }
        } catch (EOFException e) {
        }

        in.close();
        System.out.println("count=" + count);
        System.out.println("End test buffered reader");

        int bufferSize = 1 * 1024 * 1024; //1MB
        System.out.println("Test reader with 1 MB buffer");
        DataInputStream in2 = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("1.pdf"), bufferSize));
        count = 0;
        try {
            while (true) {
                byte b = in2.readByte();
                count++;
            }
        } catch (EOFException e) {
        }

        in.close();
        System.out.println("count=" + count);
        System.out.println("End test reader with 1 MB buffer");
    }

    private void testJavaFileOnCommentary() throws IOException {
        System.out.println("Test Java Commentary");
        DataInputStream in = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("Test.java")));
        while (true) {
            String line = in.readLine();
            if (line == null) break;
            int index = line.indexOf("//");
            if (index != -1)
                System.out.println(line.substring(index));
        }
        in.close();


    }

    private void showContentsDirectory() throws IOException {
        System.out.println("contents of a specific directory");
        File file = new File("D:\\MUSIC");
        if(file.exists()){
            printDirectory(file,"");
        }else System.out.println("directory do not exists");
    }

    private void printDirectory(File file, String str){
        System.out.println(str+"Directory: "+file.getName());
        str=str+"  ";
        File[] fileNames = file.listFiles();
        for(File f: fileNames){
            if (f.isDirectory()){
                printDirectory(f, str);
            }else {
                System.out.println(str+"File: "+f.getName());
            }
        }

    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
